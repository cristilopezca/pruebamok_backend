package com.mok.core.exceptions.services;

import com.mok.core.exceptions.base.ServiceException;
import com.mok.core.exceptions.enums.LogRefServices;

public class DataCorruptedServiceException extends ServiceException {
    public DataCorruptedServiceException(LogRefServices logRefServices, String message) {
        super(logRefServices, message);
    }

    public DataCorruptedServiceException(LogRefServices logRefServices, String message, Throwable cause) {
        super(logRefServices, message, cause);
    }
}
