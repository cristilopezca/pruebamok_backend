package com.mok.core.exceptions.base;

import com.mok.core.exceptions.enums.LogRefServices;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ServiceException extends RuntimeException{
    private final LogRefServices logRefServices;

    private final String message;

    public ServiceException(LogRefServices logRefServices, String message) {
        this.logRefServices = logRefServices;
        this.message = message;
    }
    public ServiceException(LogRefServices logRefServices, String message, Throwable cause) {
        super (message,cause);
        this.logRefServices = logRefServices;
        this.message = message;
    }
}
