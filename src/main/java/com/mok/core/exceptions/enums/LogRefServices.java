package com.mok.core.exceptions.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum  LogRefServices {
    LOG_REF_SERVICES("LOG_REF_SERVICES", "/ayuda/error_general_servicio.html"),
    ERROR_PERSISTENCE("ERROR_PERSISTENCE", "/ayuda/error_persistencia.html"),
    ERROR_DATA_CORRUPT("ERROR_DATA_CORRUPT", "/ayuda/error_dato_corrupto.html"),
    ERROR_DATA_NOT_FOUND("ERROR_DATA_NOT_FOUND", "/ayuda/error_general_servicio.html"),
    ERROR_SAVE("ERROR_SAVE", "/ayuda/error_general/error_guardar_solicitud.html");
    @Getter
    public final String logRef;
    @Getter
    final
    String hrefLink;
}
