package com.mok.core.exceptions.persistense;

import com.mok.core.exceptions.base.ServiceException;
import com.mok.core.exceptions.enums.LogRefServices;

public class DataCorruptedPersistenceException extends ServiceException {
    public DataCorruptedPersistenceException(LogRefServices logRefServices, String message) {
        super(logRefServices, message);
    }

    public DataCorruptedPersistenceException(LogRefServices logRefServices, String message, Throwable cause) {
        super(logRefServices, message, cause);
    }
}
