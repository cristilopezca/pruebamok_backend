package com.mok.core.exceptions.persistense;

import com.mok.core.exceptions.base.ServiceException;
import com.mok.core.exceptions.enums.LogRefServices;

public class DataNotFoundPersistenceException extends ServiceException {
    public DataNotFoundPersistenceException(LogRefServices logRefServices, String message) {
        super(logRefServices, message);
    }

    public DataNotFoundPersistenceException(LogRefServices logRefServices, String message, Throwable cause) {
        super(logRefServices, message, cause);
    }
}
