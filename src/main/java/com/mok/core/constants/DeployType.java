package com.mok.core.constants;

public class DeployType {
    public static final String DEVELOP = "dev";
    public static final String PRODUCTION = "prod";

    public DeployType() {
    }
}
