package com.mok.core.constants;

public class MessageError {
    public static final String ERROR_EN_EL_ACCESO_LA_ENTIDAD = "Error en el acceso la entidad.";
    public static final String NO_SE_HA_ENCONTRADO_LA_ENTIDAD = "No se ha encontrado la entidad.";
}
