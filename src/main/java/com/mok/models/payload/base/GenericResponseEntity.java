package com.mok.models.payload.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mok.core.exceptions.base.ServiceException;
import com.mok.core.exceptions.enums.LogRefServices;
import com.mok.models.payload.response.base.StatusDTO;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

/**
 * The type Generic response entity.
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GenericResponseEntity {

    private StatusDTO status;

    private LogRefServices errorType;

    private String detailMessage;

    private String traceRootException;

    @JsonIgnore
    private Exception rootException;

    /**
     * New builder generic response entity.
     *
     * @return the generic response entity
     */
    public static GenericResponseEntity newBuilder(){
        return new GenericResponseEntity();
    }

    /**
     * With status generic response entity.
     *
     * @param httpStatus the http status
     * @return the generic response entity
     */
    public GenericResponseEntity withStatus(HttpStatus httpStatus) {
        if (status == null) {
            this.status = new StatusDTO();
        }
        this.status.setCode(String.valueOf(httpStatus.value()));
        this.status.setDescription(httpStatus.name());
        return this;
    }

    /**
     * With error type generic response entity.
     *
     * @param exception the exception
     * @return the generic response entity
     */
    public GenericResponseEntity withErrorType(Exception exception) {
        if (exception instanceof ServiceException) {
            this.errorType = ((ServiceException) exception).getLogRefServices();
        } else {
            this.errorType = LogRefServices.LOG_REF_SERVICES;
        }
        return this;
    }

    /**
     * With detail message generic response entity.
     *
     * @param message the message
     * @return the generic response entity
     */
    public GenericResponseEntity withDetailMessage(String message) {
        this.detailMessage = message;
        return this;
    }

    /**
     * With root exception generic response entity.
     *
     * @param exception the exception
     * @return the generic response entity
     */
    public GenericResponseEntity withRootException(Exception exception) {
        Throwable trace = null;
        if (ObjectUtils.isNotEmpty(exception.getCause())) {
            trace = exception.getCause();
            if (ObjectUtils.isNotEmpty(trace.getCause())) {
                exception.getCause().getCause();
            }
        }
        this.rootException = exception;
        if (ObjectUtils.isNotEmpty(trace)) {
            this.traceRootException = StringUtils.isNotEmpty(trace.getMessage()) ? trace.getMessage()
                    .concat(" - ").concat(exception.getCause().toString()) : defaultIfNull(trace.getLocalizedMessage(), "No hay mensaje de excepción disponible.");
        }

        return this;
    }


    /**
     * Build response generic response entity.
     *
     * @return the generic response entity
     */
    public GenericResponseEntity buildResponse() {
        return this;
    }
}

