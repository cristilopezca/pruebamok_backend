package com.mok.models.payload.request;

import com.mok.models.enums.Sexo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

@Data
public class RequestEmpleado {
    @Schema(example = "Cristian")
    private String nombre;
    @Schema(example = "Lopez")
    private String apellido;
    private Date fechaNacimiento;
    private Sexo sexo;
    @Schema(example = "cristilopezca@gmail.com")
    private String correo;
}
