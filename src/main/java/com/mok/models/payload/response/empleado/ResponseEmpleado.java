package com.mok.models.payload.response.empleado;

import com.mok.models.enums.Sexo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseEmpleado {
    private Integer id;
    private String nombre;
    private String apellido;
    private Integer edad;
    private Sexo sexo;
    private String correo;
}
