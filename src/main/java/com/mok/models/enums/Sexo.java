package com.mok.models.enums;

public enum Sexo {
    Femenino (1), Masculino (2);
    private final Integer id;

    Sexo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
