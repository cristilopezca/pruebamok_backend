package com.mok.web.api;

import com.mok.models.payload.request.RequestEmpleado;
import com.mok.models.payload.response.empleado.ResponseEmpleado;
import com.mok.service.empleado.EmpleadoService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/empleado")
@CrossOrigin({"*"})
public class EmpleadoApi {
    private final EmpleadoService empleadoService;

    public EmpleadoApi(EmpleadoService empleadoService) {
        this.empleadoService = empleadoService;
    }
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ResponseEmpleado> listarTodosLosEmpleados(){
        return empleadoService.listarTodosLosEmpleados();
    }
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity<ResponseEmpleado> buscarEmpleadoPorId(@PathVariable("id")Integer id){
        ResponseEmpleado responseEmpleado = empleadoService.buscarEmpleadoPorId(id);
        return new ResponseEntity<>(responseEmpleado, new HttpHeaders(), HttpStatus.OK);
    }
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public  ResponseEntity<ResponseEmpleado> crearEmpleado(@RequestBody RequestEmpleado requestEmpleado){
        ResponseEmpleado responseEmpleado = empleadoService.crearEmpleado(requestEmpleado);
        return new ResponseEntity<>(responseEmpleado, new HttpHeaders(), HttpStatus.CREATED);
    }
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public  ResponseEntity<ResponseEmpleado> editarEmpleado(@PathVariable("id")Integer id, @RequestBody RequestEmpleado requestEmpleado){
        ResponseEmpleado responseEmpleado = empleadoService.editarEmpleado(id, requestEmpleado);
        return  new ResponseEntity<>(responseEmpleado, new HttpHeaders(), HttpStatus.CREATED);
    }
    @DeleteMapping(value = "/{id}")
    public void eliminarEmpleadoPorId(@PathVariable("id")Integer id){
        empleadoService.eliminarEmpleadoPorId(id);
    }
}
