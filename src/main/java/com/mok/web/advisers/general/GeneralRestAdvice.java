package com.mok.web.advisers.general;

import com.mok.web.advisers.base.BaseRestAdviser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * The type Person rest advicer.
 */
@Slf4j
@ControllerAdvice(assignableTypes = {

})
public class GeneralRestAdvice extends BaseRestAdviser {

}

