package com.mok.service.empleado.impl;

import com.mok.core.exceptions.enums.LogRefServices;
import com.mok.core.exceptions.persistense.DataCorruptedPersistenceException;
import com.mok.core.exceptions.persistense.DataNotFoundPersistenceException;
import com.mok.models.entity.Empleado;
import com.mok.models.payload.request.RequestEmpleado;
import com.mok.models.payload.response.empleado.ResponseEmpleado;
import com.mok.repositories.empleado.facade.EmpleadoRepositoryFacade;
import com.mok.service.empleado.EmpleadoService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.ObjectUtils.isEmpty;

@Service
public class EmpleadoServiceImpl implements EmpleadoService {
    private final EmpleadoRepositoryFacade empleadoRepositoryFacade;

    public EmpleadoServiceImpl(EmpleadoRepositoryFacade empleadoRepositoryFacade) {
        this.empleadoRepositoryFacade = empleadoRepositoryFacade;
    }
    Pattern pattern = Pattern
            .compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    @Override
    public List<ResponseEmpleado> listarTodosLosEmpleados() {
        return empleadoRepositoryFacade.listarTodosLosEmpleados().stream().map(this::responseEmpleadoEntity).collect(Collectors.toList());
    }

    @Override
    public ResponseEmpleado buscarEmpleadoPorId(Integer id) {
        return responseEmpleadoEntity(empleadoRepositoryFacade.buscarEmpleadoPorId(id));
    }

    @Override
    public ResponseEmpleado crearEmpleado(RequestEmpleado requestEmpleado) {
        if(empleadoRepositoryFacade.existsByCorreo(requestEmpleado.getCorreo())){
            throw  new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "El email ingresado ya esta registrado");
        }
        Matcher matcher = pattern.matcher(requestEmpleado.getCorreo());
        Empleado empleado = new Empleado();

        if(!(matcher.find())){
            throw new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "El email ingresado no es valido");
        }
        empleado.setNombre(requestEmpleado.getNombre());
        empleado.setApellido(requestEmpleado.getApellido());
        empleado.setSexo(requestEmpleado.getSexo());
        empleado.setFechaNacimiento(requestEmpleado.getFechaNacimiento());
        empleado.setCorreo(requestEmpleado.getCorreo());
        return responseEmpleadoEntity(empleadoRepositoryFacade.crearEmpleado(empleado));
    }

    @Override
    public ResponseEmpleado editarEmpleado(Integer id, RequestEmpleado requestEmpleado) {
        if(requestEmpleado.getNombre().isEmpty() || requestEmpleado.getApellido().isEmpty() || requestEmpleado.getCorreo().isEmpty()){
            throw new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "Los campos no pueden ir vacios");
        }
        Empleado empleado = empleadoRepositoryFacade.buscarEmpleadoPorId(id);

        empleado.setNombre(defaultIfNull(requestEmpleado.getNombre(),empleado.getNombre()));
        empleado.setApellido(defaultIfNull(requestEmpleado.getApellido(),empleado.getApellido()));
        empleado.setSexo(defaultIfNull(requestEmpleado.getSexo(),empleado.getSexo()));
        empleado.setCorreo(defaultIfNull(requestEmpleado.getCorreo(),empleado.getCorreo()));

        return responseEmpleadoEntity(empleadoRepositoryFacade.crearEmpleado(empleado));
    }

    @Override
    public void eliminarEmpleadoPorId(Integer id) {
        empleadoRepositoryFacade.eliminarEmpleadoPorId(id);
    }

    private ResponseEmpleado responseEmpleadoEntity(Empleado empleado) {

        Integer edad = obtenerEdad(empleado);

        ResponseEmpleado responseEmpleado = new ResponseEmpleado();
        responseEmpleado.setId(empleado.getId());
        responseEmpleado.setNombre(empleado.getNombre());
        responseEmpleado.setApellido(empleado.getApellido());
        responseEmpleado.setSexo(empleado.getSexo());
        responseEmpleado.setEdad(edad);
        responseEmpleado.setCorreo(empleado.getCorreo());
        return responseEmpleado;
    }

    private Integer obtenerEdad(Empleado empleado) {
        LocalDate localDate = LocalDate.now();
        LocalDate fechaDeNacimiento = empleado.getFechaNacimiento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Period periodo = Period.between(fechaDeNacimiento, localDate);
        return periodo.getYears();
    }
}
