package com.mok.service.empleado;

import com.mok.models.payload.request.RequestEmpleado;
import com.mok.models.payload.response.empleado.ResponseEmpleado;

import java.util.List;

public interface EmpleadoService {
    List<ResponseEmpleado> listarTodosLosEmpleados();
    ResponseEmpleado buscarEmpleadoPorId(Integer id);
    ResponseEmpleado crearEmpleado(RequestEmpleado requestEmpleado);
    ResponseEmpleado editarEmpleado(Integer id, RequestEmpleado requestEmpleado);
    void eliminarEmpleadoPorId(Integer id);
}
