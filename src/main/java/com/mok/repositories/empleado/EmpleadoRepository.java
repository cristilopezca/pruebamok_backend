package com.mok.repositories.empleado;

import com.mok.models.entity.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmpleadoRepository extends JpaRepository <Empleado,Integer>{
    boolean existsByCorreo(String correo);
    boolean existsById(String id);
}
