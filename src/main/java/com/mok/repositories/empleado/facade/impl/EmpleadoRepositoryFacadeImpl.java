package com.mok.repositories.empleado.facade.impl;

import com.mok.core.constants.MessageError;
import com.mok.core.exceptions.enums.LogRefServices;
import com.mok.core.exceptions.persistense.DataNotFoundPersistenceException;
import com.mok.models.entity.Empleado;
import com.mok.repositories.empleado.EmpleadoRepository;
import com.mok.repositories.empleado.facade.EmpleadoRepositoryFacade;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
@Component
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class EmpleadoRepositoryFacadeImpl implements EmpleadoRepositoryFacade {
    private final EmpleadoRepository empleadoRepository;

    public EmpleadoRepositoryFacadeImpl(EmpleadoRepository empleadoRepository) {
        this.empleadoRepository = empleadoRepository;
    }

    @Override
    public List<Empleado> listarTodosLosEmpleados() {
        try {
            return Optional.of(empleadoRepository.findAll()).orElseThrow(() -> new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, "No se encontraron empleados registrados"));
        } catch (EmptyResultDataAccessException er) {
            throw new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, MessageError.NO_SE_HA_ENCONTRADO_LA_ENTIDAD);
        } catch (DataAccessException dae) {
            throw new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, MessageError.NO_SE_HA_ENCONTRADO_LA_ENTIDAD, dae);
        }
    }

    @Override
    public Empleado buscarEmpleadoPorId(Integer id) {
        return empleadoRepository.findById(id).orElseThrow(() -> new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, "No se encontraron empleados con el id " + id));
    }

    @Override
    public Empleado crearEmpleado(Empleado empleado) {
        try {
            return empleadoRepository.save(empleado);
        } catch (EmptyResultDataAccessException er) {
            throw new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, MessageError.NO_SE_HA_ENCONTRADO_LA_ENTIDAD);
        } catch (DataAccessException dae) {
            throw new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, MessageError.NO_SE_HA_ENCONTRADO_LA_ENTIDAD, dae);
        }
    }

    @Override
    public void eliminarEmpleadoPorId(Integer id) {
        if(!(empleadoRepository.existsById(id))){
            throw  new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, "No se puede eliminar porque no existe el id");
        }
        empleadoRepository.deleteById(id);
    }

    @Override
    public boolean existsByCorreo(String correo) {
        return empleadoRepository.existsByCorreo(correo);
    }
}
