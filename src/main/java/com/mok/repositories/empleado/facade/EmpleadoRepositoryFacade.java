package com.mok.repositories.empleado.facade;

import com.mok.models.entity.Empleado;

import java.util.List;

public interface EmpleadoRepositoryFacade {
    List<Empleado> listarTodosLosEmpleados();
    Empleado buscarEmpleadoPorId(Integer id);
    Empleado crearEmpleado(Empleado empleado);
    void eliminarEmpleadoPorId(Integer id);
    boolean existsByCorreo(String correo);
}
