insert into empleado (id, nombre, apellido, fecha_nacimiento, sexo, correo)
values (1, 'Janis','Bell','1998-01-01','Femenino','janis@gmail.com');
insert into empleado (id, nombre, apellido, fecha_nacimiento, sexo, correo)
values (2, 'Frank','Miles','2002-01-01','Masculino','frank@gmail.com');
insert into empleado (id, nombre, apellido, fecha_nacimiento, sexo, correo)
values (3, 'Clifton','Crawford','1965-01-01','Masculino','clifton@gmail.com');
insert into empleado (id, nombre, apellido, fecha_nacimiento, sexo, correo)
values (4,'Cesar','Mckinney','1989-01-01','Masculino','cesar@gmail.com');
insert into empleado (id, nombre, apellido, fecha_nacimiento, sexo, correo)
values (5, 'Lorene','Dixon','1943-01-01','Femenino','lorene@gmail.com');
insert into empleado (id, nombre, apellido, fecha_nacimiento, sexo, correo)
values (6,'Tommy','Pope','1956-01-01','Masculino','tommy@gmail.com');
insert into empleado (id, nombre, apellido, fecha_nacimiento, sexo, correo)
values (7,'Rosemary','Jackson','1926-01-01','Femenino','rosemary@gmail.com');
insert into empleado (id, nombre, apellido, fecha_nacimiento, sexo, correo)
values (8,'Willie','Dennis','2000-01-01','Masculino','willie@gmail.com');
insert into empleado (id, nombre, apellido, fecha_nacimiento, sexo, correo)
values (9,'Winifred','Carroll','1998-01-01','Masculino','winifred@gmail.com');
insert into empleado (id, nombre, apellido, fecha_nacimiento, sexo, correo)
values (10,'Cynthia','Perkins','1950-01-01','Femenino','cynthia@gmail.com');